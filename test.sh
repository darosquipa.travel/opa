#!/bin/bash

echo "1. Check that alice can see her own salary:"
curl --user alice:password localhost:5000/finance/salary/alice

echo "2. Check that bob can see alice’s salary (because bob is alice’s manager.):"
curl --user bob:password localhost:5000/finance/salary/alice

echo "3. Check that bob CANNOT see charlie’s salary: bob is not charlie’s manager, so the following command will fail.:"
curl --user bob:password localhost:5000/finance/salary/charlie



